#include <stdio.h>
#include <png.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
/* Parte del codigo tomado de https://gist.github.com/niw/5963798 */

#include <stdlib.h>
#include <stdio.h>
#include <png.h>

typedef struct{
	int width;
	int height;
	int filas;
	int n;
	png_bytep *row_pointers;
	png_bytep *imagen;
}imagen_thread;
//Retorna los pixeles de la imagen, y los datos relacionados en los argumentos: ancho, alto, tipo de color (normalmente RGBA) y bits por pixel (usualemente 8 bits)
png_bytep * abrir_archivo_png(char *filename, int *width, int *height, png_byte *color_type, png_byte *bit_depth) {
	FILE *fp = fopen(filename, "rb");

	png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if(!png) abort();

	png_infop info = png_create_info_struct(png);
	if(!info) abort();

	if(setjmp(png_jmpbuf(png))) abort();

	png_init_io(png, fp);

	png_read_info(png, info);

  //resolucion y color de la image. Usaremos espacio de color RGBA
	*width      = png_get_image_width(png, info);
	*height     = png_get_image_height(png, info);
	*color_type = png_get_color_type(png, info);
	*bit_depth  = png_get_bit_depth(png, info);

  // Read any color_type into 8bit depth, RGBA format.
  // See http://www.libpng.org/pub/png/libpng-manual.txt

	if(*bit_depth == 16)
		png_set_strip_16(png);

	if(*color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_palette_to_rgb(png);

  // PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
	if(*color_type == PNG_COLOR_TYPE_GRAY && *bit_depth < 8)
		png_set_expand_gray_1_2_4_to_8(png);

	if(png_get_valid(png, info, PNG_INFO_tRNS))
		png_set_tRNS_to_alpha(png);

  // These color_type don't have an alpha channel then fill it with 0xff.
	if(*color_type == PNG_COLOR_TYPE_RGB ||
		*color_type == PNG_COLOR_TYPE_GRAY ||
		*color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

	if(*color_type == PNG_COLOR_TYPE_GRAY ||
		*color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
		png_set_gray_to_rgb(png);

	png_bytep *row_pointers;
	png_read_update_info(png, info);
	row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * (*height));
	if(row_pointers == NULL){
		printf("Error al obtener memoria de la imagen\n");
		exit(-1);
	}
	for(int y = 0; y < *height; y++) {
		row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(png,info));
	}

	png_read_image(png, row_pointers);

	fclose(fp);
	return row_pointers;
}



//Usaremos bit depth 8
//Color type PNG_COLOR_TYPE_GRAY_ALPHA
void guardar_imagen_png(char *filename, int width, int height, png_byte color_type, png_byte bit_depth, png_bytep *res) {
  //int y;

	FILE *fp = fopen(filename, "wb");
	if(!fp) abort();

	png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png) abort();

	png_infop info = png_create_info_struct(png);
	if (!info) abort();

	if (setjmp(png_jmpbuf(png))) abort();

	png_init_io(png, fp);

  // Salida es escala de grises
	png_set_IHDR(
		png,
		info,
		width, height,
		bit_depth,
		color_type,
		PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT,
		PNG_FILTER_TYPE_DEFAULT
		);
	png_write_info(png, info);

  // To remove the alpha channel for PNG_COLOR_TYPE_RGB format,
  // Use png_set_filler().
  //png_set_filler(png, 0, PNG_FILLER_AFTER);
  png_write_image(png, res);	//row_pointers
  png_write_end(png, NULL);
  for(int y = 0; y < height; y++) {
    free(res[y]);		//row_pointers
}
  free(res);			//row_pointers

  fclose(fp);
}



void *procesar_thread(void *var){
	imagen_thread *img_t = (imagen_thread *)var;
	

	for(int i = img_t->filas;i<img_t->height;i+=img_t->n){


			img_t->imagen[i] = malloc(sizeof(png_bytep)*img_t->width*2);			//greyscale con alpha, 2 bytes por pixel

			if(img_t->imagen[i] == NULL){
				printf("Malloc error");
				exit(-1);
			}

			png_bytep row = img_t->row_pointers[i];
			for(int x = 0; x < img_t->width; x++) {
				png_bytep px = &(row[x * 4]);

			  //Convertimos a escala de grises
				float a = .299f * px[0] + .587f * px[1] + .114f * px[2];
				img_t->imagen[i][2*x] = a;
			  img_t->imagen[i][2*x + 1] = px[3]; //transparencia... dejamos el campo de transparencia igual.

			}
		
		
	}

	return img_t;
	
}

png_bytep* procesar_threads(int width, int height, int n, png_bytep *row_pointers) {
	
	
	
	png_bytep *imagen = malloc(sizeof(png_bytep *) * height);
	pthread_t ids[n];
	
	for(int i = 0; i<n;i++){
		
		imagen_thread *img_t = (imagen_thread *) malloc(sizeof(imagen_thread));
		img_t->width = width;
		img_t->height = height;
		img_t->row_pointers = row_pointers;
		img_t->n = n;
		img_t->filas=i;
		img_t->imagen=imagen;
		pthread_t id;
		pthread_create(&id,NULL,procesar_thread,img_t);
		ids[i]=id;
	}
	
	for(int i=0;i<n;i++){
		void *img_t;
		pthread_join(ids[i],&img_t);
		free(img_t);
	}
	for(int i = 0 ; i<height;i++){
		free(row_pointers[i]);
	}
	free(row_pointers);
	
	return imagen;
}

double obtenerTiempoActual(){
	struct timespec tsp;
	clock_gettime(CLOCK_REALTIME, &tsp);
	double secs=(double) tsp.tv_sec;
	double nano = (double) tsp.tv_nsec/ 1000000000.0;
	return secs + nano;
}

int main(int argc, char *argv[]) {

	if(argc<5){
		printf("Uso: ./procesador ruta_fuente ruta_destino -n <# hilos>\n");
		return 1;
	}
	int n=0;
	char *entrada = argv[1];
	char *salida = argv[2];
	if(strcmp("-n",argv[3])==0){
		n = atoi(argv[4]);
	}
	if(n<=0){
		printf("Cantidad de threads invalida\n");
		return 1;
	}
  
	int width, height;
	png_byte color_type; 

	png_bytep *pixeles;

	png_byte bit_depth;

	pixeles = abrir_archivo_png(entrada,&width, &height, &color_type, &bit_depth);
	double inicio = obtenerTiempoActual();
	png_bytep *res = procesar_threads(width,height,n, pixeles);
	double final = obtenerTiempoActual();
	guardar_imagen_png(salida, width, height, PNG_COLOR_TYPE_GRAY_ALPHA, bit_depth, res);
	double diferencia = final - inicio;
	printf("Tiempo: %lf segundos\n",diferencia);
	return 0;
	
}


